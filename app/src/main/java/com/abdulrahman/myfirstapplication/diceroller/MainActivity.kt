package com.abdulrahman.myfirstapplication.diceroller
//abdulrahman Alshammari
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import android.view.View
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    fun randomize (View:View){
        var ran =(1..6).random()
        toastme("randomize")
        val textView:TextView=findViewById<TextView>(R.id.number_field)

        textView.text=ran.toString()

    }
    fun count(view:View){
        val textView:TextView=findViewById<TextView>(R.id.number_field)

        var textytext:Int = textView.text.toString().toInt()
//        Log.d("textviewtext",)
        if (textytext < 6){
            textytext+=1
        }else{
            textytext=textytext
        }
        toastme("count")
        textView.text=textytext.toString()
    }
    private fun toastme(str:String) {
        Toast.makeText(this, "$str button clicked",
            Toast.LENGTH_SHORT).show()
    }
}
